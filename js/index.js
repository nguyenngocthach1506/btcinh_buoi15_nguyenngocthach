// Bai 1: Quan Ly Sinh Vien
function DiemKhuVuc() {
    var khuVuc = document.getElementById("khuVuc").value;

    switch (khuVuc) {
        case "A":
            return 2;
        case "B":
            return 1;
        case "C":
            return 0.5;
        default:
            return 0;
    }
}
function DiemDoiTuong() {
    var doiTuong = document.getElementById("doiTuong").value;

    switch (doiTuong) {
        case "1":
            return 2.5;
        case "2":
            return 1.5;
        case "3":
            return 1;
        default:
            return 0;
    }
}
function Bai1() {
    var diem1 = document.getElementById("diem1").value * 1;
    var diem2 = document.getElementById("diem2").value * 1;
    var diem3 = document.getElementById("diem3").value * 1;
    var diemChuan = document.getElementById("diemChuan").value * 1;
    var diemUuTien = DiemKhuVuc() + DiemDoiTuong();
    var diemTongKet = (diem1 + diem2 + diem3 + diemUuTien);
    var ketQua = "";

    if (diem1 <= 0 || diem2 <= 0 || diem3 <= 0) {
        ketQua = "Rớt";
    }
    else if (diemTongKet >= diemChuan) {
        ketQua = "Đậu";
    }
    else
        ketQua = "Rớt";
    document.getElementById("kqBai1").innerText = ketQua + "!!!";
}


// Bai 2: Tính Tiền Điện
function TinhThanhTien(soKw) {
    const GIA_NHO_HON_50 = 500;
    const GIA_NHO_HON_100 = 650;
    const GIA_NHO_HON_200 = 850;
    const GIA_NHO_HON_350 = 1100;
    const GIA_CON_LAI = 1300;

    if (soKw <= 50) {
        return soKw * GIA_NHO_HON_50;
    } else
        if (soKw <= 100) {
            return 50 * GIA_NHO_HON_50 + (soKw - 50) * GIA_NHO_HON_100;
        } else
            if (soKw <= 200) {
                return 50 * GIA_NHO_HON_50 + 50 * GIA_NHO_HON_100 + (soKw - 100) * GIA_NHO_HON_200;
            } else
                if (soKw <= 350) {
                    return 50 * GIA_NHO_HON_50 + 50 * GIA_NHO_HON_100 + 100 * GIA_NHO_HON_200 + (soKw - 200) * GIA_NHO_HON_350;
                }
                else
                    return 50 * GIA_NHO_HON_50 + 50 * GIA_NHO_HON_100 + 100 * GIA_NHO_HON_200 + 150 * GIA_NHO_HON_350 + (soKw - 350) * GIA_CON_LAI;
}
function Bai2() {
    var ten = document.getElementById("ten").value;
    var soKw = document.getElementById("soKW").value * 1;
    if (soKw <= 0 || soKw == "")
        document.getElementById("kqBai2").innerText = "Nhập sai số kw";
    else {
        var tongTien = TinhThanhTien(soKw);
        document.getElementById("kqBai2").innerHTML =
            `<p>Tên Khách Hàng : ${ten}</p>
            <p>Tổng Tiền: ${tongTien.toLocaleString()} đồng</p>`;
    }
}

// Bai 3: Tính Thuế Thu Nhập
function TinhThue(tongThuNhap, soNguoi) {
    const NHO_HON_60 = 0.05;
    const NHO_HON_120 = 0.1;
    const NHO_HON_210 = 0.15;
    const NHO_HON_384 = 0.2;
    const NHO_HON_624 = 0.25;
    const NHO_HON_960 = 0.30;
    const CON_LAI = 0.35;

    var thuNhapChiuThue = tongThuNhap - 4e+6 - soNguoi * 16e+5;
    // <= 60tr
    if (thuNhapChiuThue > 0 && thuNhapChiuThue <= 60e+6) return NHO_HON_60 * thuNhapChiuThue;
    // <= 120tr
    if (thuNhapChiuThue > 60e+6 && thuNhapChiuThue <= 120e+6) return NHO_HON_120 * thuNhapChiuThue;
    // <= 210tr
    if (thuNhapChiuThue > 120e+6 && thuNhapChiuThue <= 210e+6) return NHO_HON_210 * thuNhapChiuThue;
    // <= 384tr
    if (thuNhapChiuThue > 210e+6 && thuNhapChiuThue <= 384e+6) return NHO_HON_384 * thuNhapChiuThue;
    // <= 624tr
    if (thuNhapChiuThue > 384e+6 && thuNhapChiuThue <= 624e+6) return NHO_HON_624 * thuNhapChiuThue;
    // <= 960tr
    if (thuNhapChiuThue > 624e+6 && thuNhapChiuThue <= 960e+6) return NHO_HON_960 * thuNhapChiuThue;
    // >960tr
    if (thuNhapChiuThue > 960e+6) return CON_LAI * thuNhapChiuThue;
}
function Bai3() {
    var hoTen = document.getElementById("hoTen").value;
    var tongThuNhap = document.getElementById("thuNhap").value * 1;
    var soNguoi = document.getElementById("soNguoi").value * 1;

    if (tongThuNhap <= 0 || tongThuNhap == "" || soNguoi < 0 || tongThuNhap == "")
        document.getElementById("kqBai3").innerText = "Nhập sai số!!!";
    else {
        var tienThue = TinhThue(tongThuNhap, soNguoi);
        document.getElementById("kqBai3").innerHTML = `<p>Họ Tên: ${hoTen}</p><p>Tiền Thuế = ${tienThue.toLocaleString()}đ</p>`;
    }
}


// Bài 4: Tính Tiền Cáp3
var ketNoi = document.getElementById("groupKetNoi");
ketNoi.style.display = "none";
function GroupKetNoi() {
    var loaiKH = document.getElementById("loaiKH").value * 1;
    switch (loaiKH) {
        case 2: ketNoi.style.display = "block"; break;
        default: ketNoi.style.display = "none";
    }
}

function NhaDan(soKenh) {
    if (soKenh == "" || soKenh < 0) {
        document.getElementById("kqBai4").innerText = "Chọn Lại Số Kênh !!!"; return;
    }
    const PHI_HOA_DON = 4.5;
    const PHI_DICH_VU = 20.5;
    const PHI_KENH = 7.5;
    return PHI_HOA_DON + PHI_DICH_VU + PHI_KENH * soKenh;
}

function DoanhNghiep(soKenh, soKetNoi) {
    if (soKenh == "" || soKenh < 0 || soKetNoi == "" || soKetNoi < 0) {
        document.getElementById("kqBai4").innerText = "Chọn Lại Số Kênh và Kết Nối !!!"; return;
    }
    const PHI_HOA_DON = 15;
    const PHI_DICH_VU = (soKetNoi <= 10) ? 75 : (soKetNoi - 10) * 5 + 75;
    const PHI_KENH = 50;
    return PHI_HOA_DON + PHI_DICH_VU + PHI_KENH * soKenh;
}


function Bai4() {
    var soKenh = document.getElementById("soKenh").value * 1;
    var soKetNoi = document.getElementById("soKetNoi").value * 1;
    var loaiKH = document.getElementById("loaiKH").value * 1;
    var maKH = document.getElementById("maKH").value;
    var phiCap = 0;

    switch (loaiKH) {
        case 1: phiCap = NhaDan(soKenh); break;
        case 2: phiCap = DoanhNghiep(soKenh, soKetNoi); break;
        default: document.getElementById("kqBai4").innerText = "Chọn Loại Khách Hàng!!!"; return;
    }

    document.getElementById("kqBai4").innerHTML = `<p>Mã Khách Hàng: ${maKH}</p><p>Phí Cáp = ${phiCap.toLocaleString()}$</p>`;
}